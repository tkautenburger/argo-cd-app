# Current Problems

Horizontal Pod Autoscaler (HPA) does not work. Cannot request CPU resources for no particular reasons although the metrics server works correct and the `hpa.yaml` manifest is also ok. Seems to be a known problem in some clusters with no real fix. Might be because the worker nodes are kind docker containers. 
